include .env

DOCKER_RUN=docker run --rm -v "$(PWD)":/app -w /app
NPM=$(DOCKER_RUN) node:$(NODE_VERSION) npm
PHP_IMAGE_ID=$(shell docker images -q -f reference=$(COMPOSE_PROJECT_NAME)_php)
MYSQL_CONTAINER_ID=$(shell docker ps -q -f name=$(COMPOSE_PROJECT_NAME)_mysql)


all: build

##
## Docker helpers
##

# `make start` to start all containers, or `make start c=name` to start a
# specific container.
start:
	docker-compose up -d $(c)
 
# stop containers
stop:
	docker-compose stop $(c)

# show status of all containers
status:
	docker-compose ps


##
## Build environment
##

# prepare the build environment
build-env: node_modules

# fetch node modules
node_modules:
	docker run --rm -v "$(PWD)":/app -w /app node:$(NODE_VERSION) sh -c "npm install; npm rebuild"

##
## Build project
##

# build the project
build: build-php build-node
	
# build (copy/download) PHP-dependent project components
build-php:
	mkdir -p dist
	cp -R src/{db,include,public,routes,templates} dist/
	cp src/{composer.json,composer.lock,dependencies.php,env.example} dist/

# Docker pulls images that are not already available, but we rely on an ad-hoc
# build of PHP for Composer so we must check whether it's available ourselves.
ifeq ($(PHP_IMAGE_ID), )
	docker-compose build php
endif
	docker run --rm -v "$(PWD)/dist":/app -w /app $(COMPOSE_PROJECT_NAME)_php composer install

# build Node-dependent project components
build-node: node_modules
	mkdir -p dist
	docker run --rm -v "$(PWD)":/app -w /app node:$(NODE_VERSION) npm run-script build

# initialize the containerized database
init-db:
ifeq ($(MYSQL_CONTAINER_ID), )
	$(error MySQL is not running)
endif
	docker-compose exec mysql mysql -u root -p$(MYSQL_ROOT_PASSWORD) -e \
	  "CREATE DATABASE IF NOT EXISTS $(COMPOSE_PROJECT_NAME); \
	  CREATE USER IF NOT EXISTS '$(COMPOSE_PROJECT_NAME)'@'%' IDENTIFIED BY 'password'; \
	  GRANT SELECT, INSERT, UPDATE, DELETE ON $(COMPOSE_PROJECT_NAME).* TO '$(COMPOSE_PROJECT_NAME)'@'%'"
	docker-compose exec mysql sh -c \
	  "mysql -u root -p$(MYSQL_ROOT_PASSWORD) $(COMPOSE_PROJECT_NAME) < /app/db/schema.sql"

# dump the database
dump-db:
ifeq ($(MYSQL_CONTAINER_ID), )
	$(error MySQL is not running)
endif
	docker-compose exec mysql sh -c \
	  "mysqldump -u root -p$(MYSQL_ROOT_PASSWORD) -E $(COMPOSE_PROJECT_NAME) \
	    > /app/db/schema-`date +%Y%m%d%H%M%S`.sql"


##
## Cleanup
##

# remove build artifacts
clean:
	rm -Rf dist

# remove build artifacts, build environment, containers, and volumes
# (ie: everything)
purge:
	docker-compose down -v
	rm -Rf dist node_modules


.PHONY: start stop restart status build-env build init-db dump-db clean purge
