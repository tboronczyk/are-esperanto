# are-esperanto.org

This is the source code for the *Aŭtuna Renkontiĝo de Esperanto* website.

## Hosting Requirements

The following software/services are required to host the website:

 * Nginx or Apache* with mod_rewrite
 * MySQL 8.0.1 or greater
 * PHP 7.2 or greater
 * Outbound SMTP access

\* `public/.htaccess` contains the necessary rewrite rules for Apache to route
requests through a common index file. When using nginx, the configuration must
be specified in nginx’s configuration files. See `config/nginx/default.conf`
for an example.

## Local Development

Development of this project relies on [Docker](https://docker.com/) and
[Make](https://www.gnu.org/software/make/). Be sure these programs are
installed before proceeding.

To set-up your development environment, first navigate to the root of the
project directory and copy `env.example` to `.env`. This file contains
configuration values relevant to the development environment itself (eg.
project ID, PHP version, etc). Then, execute `make build-env`.

    cd are-esperanto
    cp env.example .env
    make build-env

To build the project, execute `make build` from the project's root directory.
The build output is placed in `./dist`.

    make build
    ls ./dist/*

After building the project, it may be desirable to serve and view the website
locally. To do so, execute `make start` to start the containerized services.
Once the services are running, execute `make init-db` to initialize the
database.

    make start
    make init-db

Then, copy the `env.example` found in the output directory. This file contains
This file contains configuration values relevant to running the website, for
example MySQL connection credentials.

    cp ./dist/env.example ./dist/.env

The website will then be available at [http://localhost:8080](http://localhost:8080).
The admin page is available at [http://localhost:8080/admin](http://localhost:8080/admin).
The default credentials for admin access are `admin@example.com` and
`password`.

Be sure not to confuse the build configuration `.env` in the project's root
directory with the `.env` file in the output directory. The root directory's
file contains configuration values used by the Docker suite to provision
the development environment. The output's file contains configuration values
used by the website itself. the default values listed in both `env.example`
files are configured for local development and simply copying the files should
be sufficient.

### Makefile Targets

The `Makefile` provides the following targets:

  * `start` - start service containers (nginx, mysql, php, and mailhog)
  * `stop` - stop service containers
  * `status` - report status of running services
  * `build-env` - build the development environment
  * `build` - build the project
  * `init-db` - initialize the database (the mysql service must be running)
  * `dump-db` - dump the database to `dist/db/schema-{%Y%M%D%H%i%s}.sql` (the
    mysql service must be running)
  * `clean` - clean build artifacts
  * `purge` - clean build artifacts and tear-down development environment
  * `all` - build the development environment and project (default target)

The `start` and `stop` targets also support specifying a specific container
using `c=<name>`, for example:

    # start only the MySQL service
    make start c=mysql

## Deployment

As automated deployment has not yet been configured, these steps will guide you
through manually deploying the website. Please read through the steps first and
modify the example commands to suit your hosting environment.

  1. Execute `make build` in the project's root directory. Output is placed in
   `./dist`.

        cd are-esperanto
        make build
      
  2. Copy the contents of `./dist` to the root web directory on the hosting
   server.

        tar czf are.tgz -C dist .
        scp are.tgz username@remote-host:
        ssh username@remote-host 'tar zxf are.tgz -C /var/www'

  3. On the hosting server, copy `env.example` as `.env` and edit the
   configuration values to reflect your production environment.

        ssh username@remote-host
        cd /var/www
        cp env.example .env
        vi .env

  4. Create the website's database and a dedicated user, for example:

        CREATE DATABASE are;
        CREATE USER 'are'@'localhost' IDENTIFIED BY 'password';
        GRANT SELECT, INSERT, UPDATE, DELETE ON are.* TO 'are'@'localhost';

  5. Seed the database with `db/schema.sql`.

        mysql are < db/schema.sql

  6. Update the password (and optionally the email address) used to log in to
   the site's admin page.

        mysql are -e "UPDATE accounts SET password =
          '$(php -r "echo password_hash('password', PASSWORD_BCRYPT);")'
          WHERE email = 'admin@example.com'"
