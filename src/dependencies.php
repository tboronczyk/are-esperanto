<?php
declare(strict_types=1);

use Boronczyk\LocalizationMiddleware;
use DI\ContainerBuilder;
use Dflydev\FigCookies\Cookie;
use Dflydev\FigCookies\FigRequestCookies;
use Dflydev\FigCookies\FigResponseCookies;
use Dflydev\FigCookies\SetCookie;
use Firebase\JWT\JWT;
use GeoIp2\Database\Reader as GeoIp2Reader;
use GeoIp2\Model\Country as GeoIp2Country;
use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use ReCaptcha\ReCaptcha;
use Slim\App;
use Slim\Factory\AppFactory;
use Slim\Http\Factory\DecoratedResponseFactory;
use Slim\Psr7\Factory\ResponseFactory as SlimResponseFactory;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Twig\TwigFunction;

use ARE\Controllers\AdminController;
use ARE\Controllers\ApiController;
use ARE\Controllers\Api\AccountController as ApiAccountController;
use ARE\Controllers\Api\CrudController as ApiCrudController;
use ARE\Controllers\Api\FilesController as ApiFilesController;
use ARE\Controllers\Api\SettingsController as ApiSettingsController;
use ARE\Controllers\PageController;
use ARE\Controllers\RegistrationController;
use ARE\Middleware\AuthorizationMiddleware;
use ARE\Models\Accounts;
use ARE\Models\Authentication;
use ARE\Models\Countries;
use ARE\Models\Languages;
use ARE\Models\Pages;
use ARE\Models\Presentations;
use ARE\Models\Registrations;
use ARE\Models\Settings;
use ARE\Models\Translations;

// All object instantiation and initialization should happen in this file.

$builder = new ContainerBuilder;

$builder->addDefinitions([
    'App' => function (Container $c): App {
        AppFactory::setContainer($c);
        $app = AppFactory::create();
    
        $app->addBodyParsingMiddleware();
        $app->addRoutingMiddleware();

        $app->add($c->get('LocalizationMiddleware'));
        $app->add(TwigMiddleware::create($app, $c->get('Twig')));

        // error middleware should be registered last
        $env = $c->get('env');
        $app->addErrorMiddleware($env['DEBUG'], true, true);
    
        require_once 'routes/api.php';
        require_once 'routes/site.php';
    
        return $app;
    },

    'db' => function (Container $c): PDO {
        // avoid $c->get('env') to prevent circular dependency
        $pdo = new PDO(
            getenv('DB_DSN'),
            getenv('DB_USERNAME'),
            getenv('DB_PASSWORD')
        );
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
        if ($pdo->getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql') {
            $pdo->query('SET SESSION sql_mode = PIPES_AS_CONCAT');
        }
    
        return $pdo;
    },

    'env' => function (Container $c): array {
        $env = $_ENV;

        // normalize to bool
        $env['DEBUG'] = !(bool)strcasecmp('true', $env['DEBUG'] ?? 'false');

        // path to files directory
        $env['FILES_DIR_INTERNAL'] = realpath(getcwd() . '/public' . $env['FILES_DIR_EXTERNAL']);

	$env['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR'];
	$env['HTTP_HOST'] = $_SERVER['HTTP_HOST'];

        foreach ($c->get('Settings')->get() as $row) {
            $env[$row['param']] = $row['value'];
        }

        return $env;
    }, 

    'GeoIP' => function (Container $c): GeoIp2Country {
        $env = $c->get('env');
        $r = new GeoIp2Reader('db/GeoLite2-Country.mmdb');
        //return $r->country('174.255.66.13');
        return $r->country($env['REMOTE_ADDR']);
    },
    
    'JwtEncoder' => function (Container $c): callable {
        $env = $c->get('env');
        return function (array $token) use ($env): string {
            return JWT::encode($token, $env['JWT_SECRET']);
        };
    },

    'JwtDecoder' => function (Container $c): callable {
        $env = $c->get('env');
        return function (string $token) use ($env): array {
            return (array)JWT::decode($token, $env['JWT_SECRET'], ['HS256']);
        };
    },

    'LocalizationMiddleware' => function (Container $c): LocalizationMiddleware {
        $languages = [];
        foreach ($c->get('Languages')->get() as $row) {
            $languages[] = $row['id'];
        }
    
        $mw = new LocalizationMiddleware($languages, 'en');
        $mw->setUriParamName('l');
        $mw->setReqAttrName('lang');
        $mw->setSearchOrder([
            LocalizationMiddleware::FROM_URI_PATH,
            LocalizationMiddleware::FROM_URI_PARAM,
            LocalizationMiddleware::FROM_COOKIE,
            LocalizationMiddleware::FROM_HEADER
        ]);
        $mw->setLocaleCallback(function (string $lang) use ($c) {
            $c->get('Translations')->setLanguage($lang);
        });
        return $mw;
    },
    
    'Mailer' => function (Container $c): Swift_Mailer {
        $env = $c->get('env');
        $transport = new Swift_SmtpTransport(
            $env['SMTP_HOST'],
            $env['SMTP_PORT']
        );
        return new Swift_Mailer($transport);
    },
    
    'Message' => function (Container $c): Swift_Message {
        return new Swift_Message;
    },
    
    'ReCaptcha' => function (Container $c): ReCaptcha {
        $env = $c->get('env');
        return new ReCaptcha($env['GOOGLE_RECAPTCHA_SECRET']);
    },

    'Response' => function (Container $c): Response {
        $factory = new DecoratedResponseFactory(
            new SlimResponseFactory,
            new StreamFactory
        );
        return $factory->createResponse();
    },
    
    'RequestCookie' => function (Container $c) {
        return function (Request $req, string $name) use ($c): Cookie {
            return FigRequestCookies::get($req, $name);
        };
    },

    'SetResponseCookie' => function (Container $c) {
        return function (
            Response $resp,
            string $name,
            string $value,
            int $expires = 0,
            string $path = "",
            string $domain = "",
            bool $secure = false,
            bool $httpOnly = false
        ) use ($c): Response {
            return FigResponseCookies::set(
                $resp,
                SetCookie::create($name)
                    ->withValue($value)
                    ->withExpires($expires)
                    ->withPath($path)
                    ->withDomain($domain)
                    ->withSecure($secure)
                    ->withHttpOnly($httpOnly)
            );
        };
    },

    'Twig' => function (Container $c): Twig {
        $env = $c->get('env');
        $twig = Twig::create('templates', [
            'debug' => $env['DEBUG']
        ]);

        $twig['env'] = $env;
    
        $twigEnv = $twig->getEnvironment();

        // add __() to translate strings from template
        $twigEnv->addFunction(new TwigFunction('__', function (string $text) use ($c) {
            return $c->get('Translations')->__($text);
        }));
    
        // add isAlternateXDefault() to return whether page is xdefault
        $twigEnv->addFunction(new TwigFunction('isAlternateXDefault', function (?string $addr) use ($c) {
            // Any path that does not begin with a recognized language code is
            // considered xdefault
            foreach ($c->get('Languages')->get() as $lang) {
                if (strpos($addr ?? "", "/$lang[id]/") === 0) {
                    return false;
                }
            }
            return true;
        }));
    
        // add isIndex() to return whether page is index page
        $twigEnv->addFunction(new TwigFunction('isIndex', function (?string $addr) use ($c) {
            $addr = $addr ?? "";
            if ($addr == '') {
                return true;
            }
    
            foreach ($c->get('Languages')->get() as $lang) {
                if ($addr == "/$lang[id]/") {
                    return true;
                }
            }
            return false;
        }));
    
        return $twig;
    },
    
    // ARE middleware
    'AuthorizationMiddleware' => function (Container $c): AuthorizationMiddleware {
        return new AuthorizationMiddleware($c);
    },
    
    // ARE models
    'Accounts' => function (Container $c): Accounts {
        return new Accounts($c);
    },
    'Authentication' => function (Container $c): Authentication {
        return new Authentication($c);
    },
    'Countries' => function (Container $c): Countries {
        return new Countries($c);
    },
    'Languages' => function (Container $c): Languages {
        return new Languages($c);
    },
    'Pages' => function (Container $c): Pages {
        return new Pages($c);
    },
    'Presentations' => function (Container $c): Presentations {
        return new Presentations($c);
    },
    'Registrations' => function (Container $c): Registrations {
        return new Registrations($c);
    },
    'Settings' => function (Container $c): Settings {
        return new Settings($c);
    },
    'Translations' => function (Container $c): Translations {
        return new Translations($c);
    },
    
    // ARE controllers
    'AdminController' => function (Container $c): AdminController {
        return new AdminController($c);
    },
    'ApiController' => function (Container $c): ApiController {
        return new ApiController($c);
    },
    'ApiCrudController' => function (Container $c): ApiCrudController {
        return new ApiCrudController($c);
    },
    'ApiFilesController' => function (Container $c): ApiFilesController {
        return new ApiFilesController($c);
    },
    'ApiAccountController' => function (Container $c): ApiAccountController {
        return new ApiAccountController($c);
    },
    'ApiSettingsController' => function (Container $c): ApiSettingsController {
        return new ApiSettingsController($c);
    },
    'PageController' => function (Container $c): PageController {
        return new PageController($c);
    },
    'RegistrationController' => function (Container $c): RegistrationController {
        return new RegistrationController($c);
    }
]);

return $builder->build();    
