<?php
declare(strict_types=1);

namespace ARE\Models;

use Boronczyk\Alistair\CrudModel;
use Psr\Container\ContainerInterface as Container;

/**
 * Class Translations
 * @package ARE\Models
 */
class Translations extends CrudModel
{
    protected $lang;
    protected $strings;

    /**
     * Constructor
     *
     * @param Container $c
     */
    public function __construct(Container $c)
    {
        parent::__construct($c->get('db'));

        $this->strings = [];
    }

    /**
     * Return table columns
     *
     * @return array
     */
    public function columns(): array
    {
        return ['msgid', 'msgstr_en', 'msgstr_eo', 'msgstr_fr'];
    }

    /**
     * Set the target language for returned translated message strings.
     *
     * @param string $lang
     */
    public function setLanguage(string $lang)
    {
        $this->lang = $lang;
    }

    /**
     * Return a translated message string from the database by msgid.
     *
     * @param string $str
     * @return string
     * @throws \Exception
     */
    public function __(string $str): string
    {
        if (empty($this->strings[$this->lang])) {
            $this->loadStrings();
        }

        return $this->strings[$this->lang][$str] ?? $str;
    }

    /**
     * Load translated message strings into memory.
     *
     * @throws \Exception
     */
    protected function loadStrings()
    {
        if (!ctype_alpha($this->lang)) {
            throw new \Exception('invalid language code');
        }

        $query = sprintf(
            'SELECT msgid,
             (CASE msgstr_%1$s WHEN "" THEN msgid ELSE msgstr_%1$s END) AS msgstr
             FROM translations',
            $this->lang
        );
        $rows = $this->queryRows($query);

        $this->strings[$this->lang] = array_combine(
            array_column($rows, 'msgid'),
            array_column($rows, 'msgstr')
        );
    }
}
