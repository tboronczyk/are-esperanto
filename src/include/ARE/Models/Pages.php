<?php
declare(strict_types=1);

namespace ARE\Models;

use Boronczyk\Alistair\CrudModelInterface;
use Boronczyk\Alistair\DbAccess;
use Psr\Container\ContainerInterface as Container;

/**
 * Class Pages
 * @package ARE\Models
 */
class Pages extends DbAccess implements CrudModelInterface
{
    protected $container;

    public function __construct(Container $c)
    {
        parent::__construct($c->get('db'));

        $this->container = $c;
    }

    /**
     * Return whether the array contains all required fields to create/update
     * a page.
     *
     * @param array $data
     * @return bool
     */
    public function hasRequiredFields(array $data): bool
    {
        $langs = ['en', 'eo', 'fr'];

        $fields = array_merge(['template'], $langs);
        if (!$this->checkRequiredFields($fields, $data)) {
            return false;
        }

        $contentFields = ['path', 'title', 'description', 'content'];
        foreach ($langs as $lang) {
            if (!$this->checkRequiredFields($contentFields, $data[$lang])) {
                return false;
            }
        }

        return true;
    }

    /**
     * Return whether the given data contains all necessary columns to
     * create/update a record.
     *
     * @param array $fields
     * @param array $data
     * @return bool
     */
    protected function checkRequiredFields(array $fields, array $data): bool
    {
        $diff = array_diff($fields, array_keys($data));
        return (count($diff) == 0);
    }

    /**
     * Return a list summarizing available pages.
     *
     * This is used by the admin section to present a list of
     * available pages for editing.
     *
     * @param array $sort
     * @param int $count
     * @param int $offset
     * @return array
     * @throws \PDOException
     */
    public function get(array $sort = [], int $count = null, int $offset = null): array
    {
        $rows = $this->queryRows(
            'SELECT id, language, path, title FROM pages
             ORDER BY path ASC'
        );

        if (!$rows) {
            return [];
        }

        $pages = [];
        foreach ($rows as $row) {
            $pages[$row['language']][] = $row;
        }

        return $pages;
    }

    /**
     * Return a list of available pages for generating sitemap.xml
     *
     * @return array
     */
    public function getForSitemap(): array
    {
        $rows = $this->queryRows(
            'SELECT id, language, path, title, updated FROM pages'
        );

        if (!$rows) {
            return [];
        }

        $pages = [];
        foreach ($rows as $row) {
            $pages[$row['id']][$row['language']] = [
                'path' => $row['path'],
                'title' => $row['title'],
                'updated' => $row['updated']
            ];
        }

        return $pages;
    }

    /**
     * Return a page from the database by ID.
     *
     * This is used by the admin section to retrieving a specific page's
     * content to edit (page retrieval for public rendering is done with
     * getByPath()).
     *
     * @param int $id
     * @return array
     */
    public function getById(int $id): array
    {
        $rows = $this->queryRows(
            'SELECT
                p.id, p.language, p.path, p.title, p.description, p.content,
                t.template, p.updated
            FROM
                pages p
                JOIN page_templates t ON p.id = t.pages_id
            WHERE
                p.id = ?',
            [$id]
        );

        if (!$rows) {
            return [];
        }

        $page = [
            'id' => $rows[0]['id'],
            'template' => $rows[0]['template']
        ];
        foreach ($rows as $row) {
            $page[$row['language']] = [
                'path' => $row['path'],
                'title' => $row['title'],
                'description' => $row['description'],
                'content' => $row['content']
            ];
        }

        return $page;
    }

    /**
     * Create a new page in the database.
     *
     * @param array $data
     * @return int
     */
    public function create(array $data): int
    {
        $this->db->beginTransaction();

        $id = $this->queryValue('SELECT MAX(id) + 1 FROM pages');
        if (empty($id)) {
            $id = 1;
        }

        $rows = $this->queryRows('SELECT id AS lang FROM languages');
        foreach ($rows as $row) {
            $this->query(
                'INSERT INTO pages
                 (id, language, path, title, description, content)
                 VALUES (?, ?, ?, ?, ?, ?)',
                [
                    $id,
                    $row['lang'],
                    $data[$row['lang']]['path'],
                    $data[$row['lang']]['title'],
                    $data[$row['lang']]['description'],
                    $data[$row['lang']]['content']
                ]
            );
        }

        $this->query(
            'INSERT INTO page_templates (pages_id, template) VALUES (?, ?)',
            [$id, $data['template']]
        );

        $this->db->commit();
        return (int)$id;
    }

    /**
     * Delete a page from the database.
     *
     * @param int $id
     */
    public function delete(int $id)
    {
        $this->db->beginTransaction();
        $this->query('DELETE FROM pages WHERE id = ?', [$id]);
        $this->query('DELETE FROM page_templates WHERE pages_id = ?', [$id]);
        $this->db->commit();
    }

    /**
     * Update a page in the database.
     *
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data)
    {
        $this->db->beginTransaction();

        $rows = $this->queryRows('SELECT id AS lang FROM languages');
        foreach ($rows as $row) {
            $this->query(
                'UPDATE pages SET path = ?, title = ?, description = ?,
                 content = ? WHERE id = ? AND language = ?',
                [
                    $data[$row['lang']]['path'],
                    $data[$row['lang']]['title'],
                    $data[$row['lang']]['description'],
                    $data[$row['lang']]['content'],
                    $id,
                    $row['lang']
                ]
            );
        }

        $this->query(
            'UPDATE page_templates SET template = ? WHERE pages_id = ?',
            [$data['template'], $id]
        );

        $this->db->commit();
    }

    /**
     * Return a page by URL path.
     *
     * This is used for page rendering.
     *
     * @param string $path
     * @param string $lang
     * @return array|null
     */
    public function getByPath(string $path, string $lang): ?array
    {
        $row = $this->queryRow(
            'SELECT
                p.id, p.language, p.path, p.title, p.description, p.content,
                t.template, p.updated
            FROM
                pages p
                JOIN page_templates t ON p.id = t.pages_id
            WHERE
                (p.language = ? AND p.path = ?)
                OR (p.language || "/" || p.path) = ?',
            [$lang, $path, $path]
        );
        if (empty($row)) {
            return null;
        }

        $row['alternates'] = $this->queryRows(
            'SELECT language, path FROM pages WHERE id = ?',
            [$row['id']]
        );

        return $row;
    }

    /**
     * Replaces {{placeholders}} found in the given content.
     *
     * Placeholders are base names of files without extension found in the
     * templates directory. Those that do not resolve to a file (the file
     * doesn't exist) are left un-replaced.
     *
);
     * @param string $content
     * @param array $data
     * @return string
     */
    public function replaceTags(string $content, array $data)
    {
        $twig = $this->container->get('Twig');
        $env = $this->container->get('env');
        $registrations = $this->container->get('Registrations');

        return preg_replace_callback(
            '/\{\{([^}]+)\}\}/',
            function (array $arg) use ($data, $twig, $env, $registrations): string {
                if ($arg[1] == 'LASTMODIFIED') {
                    $localizedDateFormats = [
                        'en' => '%s %d, %d',
                        'eo' => '%2$d de %1$s %3$d',
                        'fr' => '%2$d %1$s %3$d'
                    ];
                    $localizedMonthNames = [
                        'en' => [
                            1 => 'January', 'February', 'March', 'April', 'May',
                            'June', 'July', 'August', 'September', 'October',
                            'November`', 'December'
                        ],
                        'eo' => [
                            1 => 'januaro', 'februaro', 'marto', 'aprilo', 'majo',
                            'junio', 'julio', 'aŭgusto', 'septembro', 'oktobro',
                            'novembro', 'decembro'
                        ],
                        'fr' => [
                            1 => 'janvier', 'février', 'mars', 'avril', 'mai',
                            'juin', 'juillet', 'août', 'septembre', 'octobre',
                            'novembre', 'decembre'
                        ]
                    ];

                    $lang = $data['page']['language'];
                    list($month, $day, $year) = explode(
                        '|',
                        date('n|j|Y', strtotime($data['page']['updated']))
                    );
                    $month = $localizedMonthNames[$lang][$month];

                    return vsprintf(
                        $localizedDateFormats[$lang],
                        [$month, $day, $year]
                    );
                }
                if ($arg[1] == 'PARTICIPANTS') {
                    $participants = $registrations->getPublic($data['page']['language']);
                    return $twig->fetch('site/participants.html', ['participants' => $participants]);
                }
                if ($arg[1] == 'PROGRAM') {
                    return $this->getProgramContent($twig);
                }
                if ($arg[1] == 'REGISTRATION_BUTTON') {
                    return $twig->fetch('site/registration-button.html', $data);
                }
                if ($arg[1] == 'REGISTRATION_FORM') {
                    return $twig->fetch('site/registration-form.html', $data);
                }
                if (array_key_exists($arg[1], $env)) {
                    return $env[$arg[1]];
                }
                $file = 'site/' . trim($arg[1]);
                return (file_exists("templates/$file")) ?
                    $twig->fetch($file, $data) : $arg[0];
            },
            $content
        );
    }

    protected function getProgramContent($twig)
    {
        $presentations = $this->container->get('Presentations');
        $program = $presentations->get();
        return $twig->fetch("site/program.html", ['program' => $program]);
    }
}
