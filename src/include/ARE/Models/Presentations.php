<?php
declare(strict_types=1);

namespace ARE\Models;

use Psr\Container\ContainerInterface as Container;
use Boronczyk\Alistair\CrudModel;

/**
 * Class Presentations
 * @package Swsk\Models
 */
class Presentations extends CrudModel
{
    protected $container;

    /**
     * Constructor
     *
     * @param Container $c
     */
    public function __construct(Container $c)
    {
        parent::__construct($c->get('db'));
        $this->container = $c;
    }

    public function columns(): array
    {
        return [
            'day',
            'time',
            'location',
            'presenter',
            'image',
            'title',
            'description'
        ];
    }

    public function get(array $sort = [], int $count = null, int $offset = null): array
    {
        $presentations = $this->queryRows(
            'SELECT id, `day`, `time`, location, presenter, image, title, description
            FROM presentations'
        );
        usort($presentations, function ($a, $b) {
            return strnatcmp($a['day'].$a['time'].$a['location'], $b['day'].$b['time'].$b['location']);
        });
        return $presentations;
    }



}
