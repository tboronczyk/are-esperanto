<?php
declare(strict_types=1);

namespace ARE\Models;

use Boronczyk\Alistair\CrudModel;
use Psr\Container\ContainerInterface as Container;

/**
 * Class Registrations
 * @package ARE\Models
 */
class Registrations extends CrudModel
{
    protected $container;

    public function __construct(Container $c)
    {
        parent::__construct($c->get('db'));
        $this->container = $c;
    }

    public function columns(): array
    {
        return ['first_name', 'last_name', 'email', 'country', 'phone',
            'lodging', 'public'];
    }

    /**
     * Return a list of registration entries.
     *
     * This is used in the admin section to display the master registration
     * list. See getPublic() for the public facing list.
     *
     * @param array $sort
     * @param int $count
     * @param int $offset
     * @return array
     * @throws \PDOException
     */
    public function get(array $sort = [], int $count = null, int $offset = null): array
    {
        return $this->queryRows(
            'SELECT
                id, first_name, last_name, country, email, phone, lodging,
                public, `primary`, created
            FROM
                registrations
            WHERE
                confirmed = 1
            ORDER BY
                last_name, first_name'
        );
    }

    /**
     * Return a list of registration entries suitable for public use.
     *
     * The country name is translated to the target language.
     *
     * @param string $lang target langauge
     * @return array
     */
    public function getPublic(string $lang): array
    {
        $registrations = $this->get();
        $countries = $this->container->get('Countries')->listNamesByCode($lang);

        $public = [];
        foreach ($registrations as $r) {
            if ($r['public']) {
                $public[] = [
                    'first_name' => $r['first_name'],
                    'last_name' => $r['last_name'],
                    'country' => $countries[$r['country']]
                ];
            }
        }

        return $public;
    }

    /**
     * Return a registration entry from the database by ID.
     *
     * @param int $id
     * @return array
     */
    public function getById(int $id): array
    {
        return $this->queryRow(
            'SELECT
                id, first_name, last_name, country, email, phone, lodging,
                public, DATE(created) AS created
            FROM
                registrations
            WHERE
                id = ?',
            [$id]
        );
    }

    /**
     * Return a registration's guest entries from the database by the primary's ID.
     *
     * @param int $id
     * @return array
     */
    public function guestsByPrimaryId(int $id): array
    {
        return $this->queryRows(
            'SELECT
                id, first_name, last_name, country, email, phone, lodging,
                public, DATE(created) AS created
            FROM
                registrations
            WHERE
                `primary` = ?',
            [$id]
        );
    }

    /**
     * Create a new registration entry in the database.
     *
     * @param array $data
     * @return int
     */
    public function create(array $data): int
    {
        $this->query(
            'INSERT INTO registrations
             (id, first_name, last_name, email, country, lodging, phone,
              public, `primary`, confirmed, ipaddr)
             VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
            [
                $data['first_name'],
                $data['last_name'],
                $data['email'],
                $data['country'],
                $data['lodging'],
                $data['phone'],
                $data['public'],
                $data['primary'] ?? 0,
                0,
                $this->container->get('env')['REMOTE_ADDR']
            ]
        );
        return (int)$this->db->lastInsertId();
    }

    /**
     * Create a new pending registration entry in the database.
     *
     * @param array $data
     * @return array
     */
    public function createPending(array $data): array
    {
        $this->db->beginTransaction();

        // register primary
        unset($data['primary']);
        $primaryId = $this->create($data);

        // register additional guests
        if (!empty($data['guests'])) {
            $data['primary'] = $primaryId;
            foreach ($data['guests'] as $guest) {
                $guest = array_merge($data, $guest);
                $this->create($guest);
            }
        }

        // generate and save confirmation code
        $code = sha1(uniqid("$primaryId"));
        $this->query(
            'INSERT INTO pending_codes (id, code) VALUES (?, ?)',
            [$primaryId, $code]
        );

        $this->db->commit();

        return ['uid' => $primaryId, 'code' => $code];
    }

     /**
     * Confirm a pending registration entry.
     *
     * @param int $id
     * @param string $code
     * @return bool
     */
    public function confirm(int $id, string $code): bool
    {
        $this->db->beginTransaction();

        $count = $this->queryValue(
            'SELECT COUNT(id) FROM pending_codes WHERE id = ? AND code = ?',
            [$id, $code]
        );

        if (((int)$count) != 1) {
            $this->db->rollback();
            return false;
        }

        $this->query(
            'UPDATE registrations SET confirmed = 1 WHERE id = ? OR `primary` = ?',
            [$id, $id]
        );

        $this->query(
            'DELETE FROM pending_codes WHERE id = ? AND code = ?',
            [$id, $code]
        );

        $this->db->commit();

        return true;
    }
}
