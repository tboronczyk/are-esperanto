<?php
declare(strict_types=1);

namespace ARE\Models;

use Boronczyk\Alistair\DbAccess;
use Psr\Container\ContainerInterface as Container;

/**
 * Class Languages
 * @package ARE\Models
 */
class Languages extends DbAccess
{
    public function __construct(Container $c)
    {
        parent::__construct($c->get('db'));
    }

    /**
     * Return a list of registered languages.
     *
     * @return array
     * @throws \PDOException
     */
    public function get(): array
    {
        return $this->queryRows(
            'SELECT id, name FROM languages ORDER BY name ASC'
        );
    }
}
