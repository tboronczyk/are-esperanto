<?php
declare(strict_types=1);

namespace ARE\Models;

use Boronczyk\Alistair\DbAccess;
use Psr\Container\ContainerInterface as Container;

/**
 * Class Countries
 * @package ARE\Models
 */
class Countries extends DbAccess
{
    public function __construct(Container $c)
    {
        parent::__construct($c->get('db'));
    }

    /**
     * Return a list of country codes and names in the target language.
     *
     * @param string $lang
     * @return array
     */
    public function getByLanguage(string $lang): array
    {
        return $this->queryRows(
            "SELECT id AS code, name FROM countries WHERE language = ?
             ORDER BY name ASC",
            [$lang]
        );
    }

    /**
     * Return a list of country names indexed by country code.
     *
     * @param string $lang
     * @return array
     */
    public function listNamesByCode(string $lang): array
    {
        $countries = $this->queryRows(
            "SELECT name, id FROM countries WHERE language = ?
             ORDER BY name ASC",
            [$lang]
        );

        return array_combine(
            array_column($countries, 'id'),
            array_column($countries, 'name')
        );
    }
}
