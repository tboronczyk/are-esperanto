<?php
declare(strict_types=1);

namespace ARE\Controllers;

use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class PageController
 * @package ARE\Controllers
 */
class PageController extends Controller
{
    protected $pages;

    public function __construct(Container $c)
    {
        parent::__construct($c);
        $this->pages = $c->get('Pages');
    }

    /**
     * Serve the site admin page
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function getAdmin(Request $req, Response $resp, array $args): Response
    {
        return $this->twig->render($resp, 'admin/app.html');
    }

    /**
     * Serve robots.txt
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function getRobots(Request $req, Response $resp, array $args): Response
    {
        return $this->twig->render(
            $resp->withHeader('Content-Type', 'text/plain'),
            'site/robots.txt'
        );
    }

    /**
     * Serve sitemap.xml
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function getSitemap(Request $req, Response $resp, array $args): Response
    {
        return $this->twig->render(
            $resp->withHeader('Content-Type', 'application/xml'),
            'site/sitemap.xml',
            ['pages' => $this->pages->getForSitemap()]
        );
    }

    /**
     * Serve the requested page.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function get(Request $req, Response $resp, array $args): Response
    {
        $lang = $req->getAttribute('lang');

        $page = $this->pages->getByPath($args['path'], $lang);
        if (!$page) {
            // requested page does not exist
            $page = $this->pages->getByPath("$lang/404", $lang);
            if (!$page) {
                // 404 page doesn't exist!
                return $this->serverErrorResponse($resp);
            }
            $resp = $resp->withStatus(404);
        }

        $data = [
            'page' => $page,
            'languages' => $this->container->get('Languages')->get()
        ];

        $data['page']['content'] = $this->pages->replaceTags($page['content'], $data);

        return $this->twig->render($resp, "site/$page[template].html", $data);
    }
}
