<?php
declare(strict_types=1);

namespace ARE\Controllers;

use ARE\Models\Registrations;
use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class RegistrationController
 * @package ARE\Controllers
 */
class RegistrationController extends Controller
{
    protected $mailer;
    protected $registration;

    public function __construct(Container $c)
    {
        parent::__construct($c);

        $this->mailer = $c->get('Mailer');
        $this->registration = $c->get('Registrations');
    }

    /**
     * Handle submission of the registration form.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function post(Request $req, Response $resp, array $args): Response
    {
        $data = $req->getParsedBody();
        $data['public'] = (int)isset($data['consent-list']);

        // validate primary registrant's data
        if (!$this->registration->hasRequiredFields($data)) {
            return $this->invalidFormResponse($resp);
        }


        // validate additional registrants' names
        $guests = [];
        if (!empty($data['guests'])) {
            foreach ($data['guests'] as $guest) {
                // skip empty guests
                if (empty($guest['first_name']) && empty($guest['last_name'])) {
                    continue;
                }
                // if either the first or last name is set without the other
                // then the guest is invalid
                if (empty($guest['first_name']) xor empty($guest['last_name'])) {
                    return $this->invalidFormResponse($resp);
                }
                $guests[] = $guest;
            }
        }

        $env = $this->container->get('env');
        if (!empty($env['GOOGLE_RECAPTCHA_KEY'])) {
            $captcha = $this->container->get('ReCaptcha');
            $verify = $captcha->verify($data['g-recaptcha-response'], $env['REMOTE_ADDRESS']);
            if (!$verify->isSuccess()) {
                return $this->captchaFailedResponse($resp);
            }
        }

        // create pending registration
        $registration = $this->registration->createPending($data);

        // send confirmation message to the user
        $data = array_merge($data, $registration);
        $data['lang'] = $req->getAttribute('lang');

        switch ($data['lodging']) {
            case 'inn':
                $data['lodging'] = 'The Inn';
                break;

            case 'daypass':
                $data['lodging'] = $this->translations->__('Form: Day Pass');
                break;

            case 'none':
            default:
                $data['lodging'] = $this->translations->__('Form: None');
                break;
        }

        $this->sendRegistrationPending($data);

        return $this->twig->render($resp, "site/registration-pending.{$data['lang']}.html", $data);
    }

    /**
     * Handle confirmation of the registration request.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function confirm(Request $req, Response $resp, array $args): Response
    {
        $id = (int)$args['uid'];
        $code = $args['code'];

        $success = $this->registration->confirm($id, $code);

        if (!$success) {
            $page = '/' . $req->getAttribute('lang') . '/' . $this->translations->__('Page: Register Fail');
            return $resp->withHeader('Location', $page);
        }

        $data = $this->registration->getById($id);
        $data['guests'] = $this->registration->guestsByPrimaryId($id);
        $data['lang'] = $req->getAttribute('lang');

        if ($data['lodging'] == 'inn') {
            // send lodging request to YMCA
            $this->sendReservationRequest($data);
        }

        if ($data['lodging'] == 'inn') {
            $data['lodging'] = 'The Inn';
            $data['showReservationNotConfirmed'] = true;
        } else {
            $data['lodging'] = $this->translations->__('Form: None');
            $data['showReservationNotConfirmed'] = false;
        }

        $this->sendRegistrationConfirmed($data);

        $page = '/' . $data['lang'] . '/' . $this->translations->__('Page: Register Confirmed');
        return $resp->withHeader('Location', $page);
    }

    /**
     * Send the message for a pending registration to the user.
     *
     * @param array $data
     * @return bool
     */
    protected function sendRegistrationPending(array $data): bool
    {
        $env = $this->container->get('env');
        $emailAddress = $env['EMAIL_SYSTEM_ADDRESS'];
        $emailName = $env['EMAIL_SYSTEM_NAME'];

        $message = $this->container->make('Message')
            ->setTo([$data['email'] => $data['first_name'] . ' ' . $data['last_name']])
            ->setFrom([$emailAddress => $emailName])
            ->setSubject($this->translations->__('Email: Registration pending'))
            ->setBody($this->twig->fetch("mail/registration-pending.${data['lang']}.html", $data), 'text/html')
            ->addPart($this->twig->fetch("mail/registration-pending.${data['lang']}.txt", $data), 'text/plain');

        return (bool)($this->mailer->send($message));
    }

    /**
     * Send the message for a confirmed registration to the user.
     *
     * @param array $data
     * @return bool
     */
    protected function sendRegistrationConfirmed(array $data): bool
    {
        $env = $this->container->get('env');
        $emailAddress = $env['EMAIL_SYSTEM_ADDRESS'];
        $emailName = $env['EMAIL_SYSTEM_NAME'];

        $message = $this->container->make('Message')
            ->setTo([$data['email'] => $data['first_name'] . ' ' . $data['last_name']])
            ->setFrom([$emailAddress => $emailName])
            ->setSubject($this->translations->__('Email: Registration confirmed'))
            ->setBody($this->twig->fetch("mail/registration-confirmed.${data['lang']}.html", $data), 'text/html')
            ->addPart($this->twig->fetch("mail/registration-confirmed.${data['lang']}.txt", $data), 'text/plain');

        return (bool)($this->mailer->send($message));
    }

    /**
     * Send the reservation request to Silver Bay.
     *
     * @param array $data
     * @return bool
     */
    protected function sendReservationRequest(array $data): bool
    {
        $env = $this->container->get('env');
        $emailAddress = $env['EMAIL_RESERVATION_ADDRESS'];
        $emailName = $env['EMAIL_RESERVATION_NAME'];

        $emailSystemAddress = $env['EMAIL_SYSTEM_ADDRESS'];
        $emailSystemName = $env['EMAIL_SYSTEM_NAME'];

        $message = $this->container->make('Message')
            ->setTo([$emailAddress => $emailName])
            ->setFrom([$emailSystemAddress => $emailSystemName])
            ->setReplyTo([$data['email'] => $data['first_name'] . ' ' . $data['last_name']])
            ->setSubject('Reservation request')
            ->setBody($this->twig->fetch('mail/reservation-request.html', $data), 'text/html')
            ->addPart($this->twig->fetch('mail/reservation-request.txt', $data), 'text/plain');

        return (bool)($this->mailer->send($message));
    }
}

