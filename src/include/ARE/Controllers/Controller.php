<?php
declare(strict_types=1);

namespace ARE\Controllers;

use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Class Controller
 * @package ARE\Controllers
 */
class Controller
{
    protected $container;
    protected $translations;
    protected $twig;

    public function __construct(Container $c)
    {
        $this->container = $c;
        $this->twig = $c->get('Twig');
        $this->translations = $c->get('Translations');
    }

    /**
     * Return a bad request response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function badRequestResponse(Response $resp, ?string $msg = null): Response
    {
        return $resp->withJson(['error' => $msg ?? 'bad request'], 400);
    }

    /**
     * Return a failed captcha response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function captchaFailedResponse(Response $resp, ?string $msg = null): Response
    {
        return $resp->withJson(['error' => $msg ?? 'captcha failed'], 412);
    }

    /**
     * Return an unauthorized response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function unauthorizedResponse(Response $resp, ?string $msg = null): Response
    {
        return $resp->withJson(['error' => $msg ?? 'Unauthorized'], 401);
    }

    /**
     * Return a not found response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function notFoundResponse(Response $resp, ?string $msg = null): Response
    {
        return $resp->withJson(['error' => $msg ?? 'Not found'], 404);
    }

    /**
     * Return a rate limit exceeded response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function rateLimitExceededResponse(Response $resp, ?string $msg = null): Response
    {
        return $resp->withJson(['error' => $msg ?? 'Rate limit exceeded'], 429);
    }

    /**
     * Return a server error response.
     *
     * @param Response $resp
     * @param ?string $msg
     * @return Response
     */
    protected function serverErrorResponse(Response $resp, ?string $msg = null): Response
    {
        return $resp->withJson(['error' => $msg ?? 'Server error occurred'], 500);
    }
}
