<?php
declare(strict_types=1);

namespace ARE\Controllers\Api;

use ARE\Controllers\Controller;
use Boronczyk\Alistair\CrudModelInterface;
use Interop\Container\Exception\ContainerException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class CrudController
 * @package ARE\Controllers\API
 */
class CrudController extends Controller
{
    // allowed models
    protected $allowed = [
        'Accounts',
        'Pages',
        'Presentations',
        'Registrations',
        'Settings',
        'Translations'
    ];

    /**
     * Return a model for the given type name.
     *
     * Models must implement CrudModelInterface and be whitelisted in $allowed.
     *
     * @param string $type
     * @return mixed
     * @throws ContainerException|DomainException
     */
    protected function getCrudModel(string $type): CrudModelInterface
    {
        $name = ucfirst(strtolower($type));
        if (!in_array($name, $this->allowed)) {
            throw new \DomainException('"' . $type . '" is unsupported');
        }

        return $this->container->get($name);
    }

    /**
     * Return a list summarizing available records in the database.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function list(Request $req, Response $resp, array $args): Response
    {
        try {
            $model = $this->getCrudModel($args['type']);
        } catch (ContainerException | \DomainException $e) {
            return $this->badRequestResponse($resp, $e->getMessage());
        }

        try {
            $list = $model->get();
        } catch (\PDOException $e) {
            return $this->serverErrorResponse($resp, $e->getMessage());
        }

        return $resp->withJson($list);
    }

    /**
     * Return a specific record from the database.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function get(Request $req, Response $resp, array $args): Response
    {
        try {
            $model = $this->getCrudModel($args['type']);
        } catch (ContainerException | \DomainException $e) {
            return $this->badRequestResponse($resp, $e->getMessage());
        }

        try {
            $record = $model->getById((int)$args['id']);
        } catch (\PDOException $e) {
            return $this->serverErrorResponse($resp, $e->getMessage());
        }

        if (empty($record)) {
            return $this->notFoundResponse($resp);
        }

        return $resp->withJson($record);
    }

    /**
     * Create a new record in the database.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function create(Request $req, Response $resp, array $args): Response
    {
        try {
            $model = $this->getCrudModel($args['type']);
        } catch (ContainerException | \DomainException $e) {
            return $this->badRequestResponse($resp, $e->getMessage());
        }

        $data = $req->getParsedBody();
        if (!$model->hasRequiredFields($data)) {
            return $this->badRequestResponse($resp, 'Missing required fields');
        }

        try {
            $id = $model->create($data);
        } catch (\PDOException $e) {
            return $this->serverErrorResponse($resp, $e->getMessage());
        }

        return $resp->withJson(['id' => $id], 201);
    }

    /**
     * Update a record in the database.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function update(Request $req, Response $resp, array $args): Response
    {
        try {
            $model = $this->getCrudModel($args['type']);
        } catch (ContainerException | \DomainException $e) {
            return $this->badRequestResponse($resp, $e->getMessage());
        }

        $data = $req->getParsedBody();
        if (!$model->hasRequiredFields($data)) {
            return $this->badRequestResponse($resp, 'Missing required fields');
        }

        $id = (int)$args['id'];
        try {
            $record = $model->getById($id);
        } catch (\PDOException $e) {
            return $this->serverErrorResponse($resp, $e->getMessage());
        }

        if (empty($record)) {
            return $this->notFoundResponse($resp);
        }

        try {
            $model->update($id, $data);
        } catch (\PDOException $e) {
            return $this->serverErrorResponse($resp, $e->getMessage());
        }

        return $resp;
    }

    /**
     * Delete a record from the database.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function delete(Request $req, Response $resp, array $args): Response
    {
        try {
            $model = $this->getCrudModel($args['type']);
        } catch (ContainerException | \DomainException $e) {
            return $this->badRequestResponse($resp, $e->getMessage());
        }

        $id = (int)$args['id'];
        try {
            $record = $model->getById($id);
        } catch (\PDOException $e) {
            return $this->serverErrorResponse($resp, $e->getMessage());
        }

        if (empty($record)) {
            return $this->notFoundResponse($resp);
        }

        try {
            $model->delete($id);
        } catch (\PDOException $e) {
            return $this->serverErrorResponse($resp, $e->getMessage());
        }

        return $resp;
    }
}
