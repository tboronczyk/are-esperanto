<?php
declare(strict_types=1);

namespace ARE\Controllers\Api;

use ARE\Controllers\Controller;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class SettingsController
 * @package ARE\Controllers\Api
 */
class SettingsController extends CrudController
{
    /**
     * Update system settings.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function updateSettings(Request $req, Response $resp, array $args): Response
    {
        $data = $req->getParsedBody();
        $settings = $this->container->get('Settings');
        $settings->updateBulk($data);

        return $resp;
    }
}
