<?php
declare(strict_types=1);

namespace ARE\Controllers\Api;

use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class FilesController
 * @package ARE\Controllers\API
 */
class FilesController
{
    protected $container;
 
    /**
     * Constructor
     *
     * @param Container $c
     */
    public function __construct(Container $c)
    {
        $this->container = $c;
    }

    /**
     * Return a list of existing files.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function list(Request $req, Response $resp, array $args): Response
    {
        clearstatcache();

        $env = $this->container->get('env');
        $files = scandir($env['FILES_DIR_INTERNAL']);

        $filelist = [];
        foreach ($files as $filename) {
            $filepath = $env['FILES_DIR_INTERNAL'] . '/' . $filename;
            $filetype = filetype($filepath);
            if ($filetype == 'dir' || strpos($filename, '.') === 0) {
                continue;
            }

            $fileinfo = stat($filepath);
            $filelist[] = [
                'name' => $env['FILES_DIR_EXTERNAL'] . '/' . $filename,
                'size' => $fileinfo['size'],
                'date' => $fileinfo['ctime']
            ];
        }

        return $resp->withJson($filelist);
    }

    /**
     * Upload a new file.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function create(Request $req, Response $resp, array $args): Response
    {
        $env = $this->container->get('env');

        $data = $req->getParsedBody();
        $filename = $data['filename'];
        $content = $data['content'];

        // sanitize filename
        $filename = preg_replace('/[^.\w]/', '_', $filename);
        $base = substr($filename, 0, strrpos($filename, '.'));
        $ext = substr($filename, strrpos($filename, '.'));

        $i = 1;
        $filename = $base . $ext;
        while (file_exists($env['FILES_DIR_INTERNAL'] . '/' . $filename)) {
            $filename = $base . '-' . $i . $ext;
            $i++;
        }

        // process content
        list($front, $data) = explode(',', $content, 2);
        if (stristr($front, ';base64') !== false) {
            $data = base64_decode($data);
        }
        $content = $data;


        $filepath = $env['FILES_DIR_INTERNAL'] . '/' . $filename;

        file_put_contents($filepath, $content);

        $fileinfo = stat($filepath);
        $file = [
            'name' => $env['FILES_DIR_EXTERNAL'] . '/' . $filename,
            'size' => $fileinfo['size'],
            'date' => $fileinfo['mtime']
        ];

        return $resp->withJson($file, 201);
    }
}
