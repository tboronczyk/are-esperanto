<?php
declare(strict_types=1);

namespace ARE\Controllers\Api;

use ARE\Controllers\Controller;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class AccountController
 * @package ARE\Controllers\Api
 */
class AccountController extends CrudController
{
    /**
     * Update the user's password.
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function updatePassword(Request $req, Response $resp, array $args): Response
    {
        $account = $req->getAttribute('account');
        $data = $req->getParsedBody();

        $auth = $this->container->get('Authentication');
        if (!$auth->authenticate($account['email'], $data['currentPassword'])) {
            return $this->badRequestResponse($resp, 'Current password is invalid');
        }

        $accounts = $this->container->get('Accounts');
        $accounts->updatePassword((int)$account['id'], $data['newPassword']);

        return $resp;
    }
}
