<?php
declare(strict_types=1);

namespace ARE\Middleware;

use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;

/**
 * Class AuthorizationMiddleware
 * @package ARE\Middleware
 */
class AuthorizationMiddleware implements MiddlewareInterface
{
    protected $container;

    public function __construct(Container $c)
    {
        $this->container = $c;
    }

    /**
     * Accept JWT and block unauthenticated requests.
     *
     * @param Request $req
     * @param RequestHandler $handler
     * @return Response
     */
    public function process(Request $req, RequestHandler $handler): Response
    {
        $header = $req->getHeaderLine('Authorization');
        $jwt = trim((string)substr($header, strlen('Bearer ')));

        try {
            $authToken = $this->container->get('JwtDecoder')($jwt);

            if ($authToken['exp'] < time()) {
                throw new \Exception('Expired session');
            }
        }
        catch (\Exception $e) {
            $resp = $this->container->get('Response')
                ->withStatus(401)
                ->withHeader('WWW-Authenticate', 'Bearer')
                ->withHeader('Content-Type', 'application/json');

            $resp->getBody()
                ->write(json_encode(['error' => 'Expired session']));

            return $resp;
        }

        $req = $req->withAttribute('account', (array)($authToken['account']));

        return $handler->handle($req);
    }
}
