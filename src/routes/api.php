<?php
declare(strict_types=1);

use Slim\Routing\RouteCollectorProxy;

/** @var $app Slim\App */

$app->group('/api', function (RouteCollectorProxy $group) use ($app) {

    $group->get('/countries/{lang:.+}', 'ApiController:getCountries');
    $group->get('/languages', 'ApiController:getLanguages');

    $group->group('/auth', function (RouteCollectorProxy $group) use ($app) {
        $group->post('', 'ApiController:auth');
        $group->delete('', 'ApiController:deauth');

        $group->post('/renew', 'ApiController:renew');
    });

    $group->group('', function (RouteCollectorProxy $group) use ($app) {

        $group->post('/password', 'ApiAccountController:updatePassword');
        $group->post('/settings', 'ApiSettingsController:updateSettings');

        $group->group('/files', function (RouteCollectorProxy $group) use ($app) {
            $group->get('', 'ApiFilesController:list');
            $group->post('', 'ApiFilesController:create');
        });

        // basic CRUD handling
        $group->group('/{type}', function (RouteCollectorProxy $group) use ($app) {
            $group->get('', 'ApiCrudController:list');
            $group->post('', 'ApiCrudController:create');
            $group->get('/{id:\d+}', 'ApiCrudController:get');
            $group->put('/{id:\d+}', 'ApiCrudController:update');
            $group->delete('/{id:\d+}', 'ApiCrudController:delete');
        });
    })->add($app->getContainer()->get('AuthorizationMiddleware'));
});
