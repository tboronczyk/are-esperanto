<?php
declare(strict_types=1);

/** @var $app Slim\App */

$app->get('/admin[/{page:\w+}]', 'PageController:getAdmin');

$app->get('/robots.txt', 'PageController:getRobots');
$app->get('/sitemap.xml', 'PageController:getSitemap');

// registration confirmation
$app->get('/{lang:en|eo|fr}/rc/{uid:\d+}/{code:\w+}', 'RegistrationController:confirm');

$app->get('/{path:.*}', 'PageController:get');

$app->post('/register', 'RegistrationController:post');
