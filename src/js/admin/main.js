import axios from "axios";
import Buefy from "buefy";
import {NotificationProgrammatic as Notification} from "buefy";
import Vue from "vue";
import VueElementLoading from "vue-element-loading";

import App from "./App.vue";
import router from "./router";
import store from "./store";

import "vue2-animate/dist/vue2-animate.min.css";
import "@fortawesome/fontawesome-free/js/all.js";
// import "buefy/dist/buefy.css";

Vue.use(Buefy, {
    defaultToastDuration: 3000
});

Vue.component("VueElementLoading", VueElementLoading);

Vue.prototype.$http = axios;

// Attach authorization header to requests if authToken exists
// and set loading indicator
axios.interceptors.request.use((request) => {
    const authToken = store.getters["auth/authToken"];
    if (authToken) {
        request.headers.Authorization = "Bearer " + authToken;
    }
    return request;
});

// Clear loading indicator, Attempt to renew an expired authToken
// and re-issue a failed request
axios.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        // take no action for auth routes and non-noauth failures
        if (error.config.url.startsWith("/api/auth") ||
            error.response.status != 401
        ) {
            return Promise.reject(error);
        }

        return axios.post("/api/auth/renew")
            .then((response) => {
                return store.dispatch("auth/setAuthToken", response.data.authToken)
                    .then(() => {
                        return axios.request(error.config);
                    })
            })
            .catch((error) => {
                return Promise.reject(error);
            });
    }
);

// clear authToken and redirect to login page
const expireSession = (message) => {
    store.dispatch("auth/setAuthToken", "")
        .finally(() => {
            router.push({name: "login", params: {internal: true}});
            Notification.open({
                message,
                indefinite: true,
                position: "is-top",
                type: "is-warning"
            });
        });
};

// listen for login/out actions from other tabs
window.addEventListener("storage", (evt) => {
    if (evt.key == "login" && evt.newValue) {
        if (router.currentRoute.name != "login") {
            expireSession("This session expired because you’ve logged in from another tab.");
        }
        return;
    }

    if (evt.key == "logout" && evt.newValue) {
        if (router.currentRoute.name != "login") {
            expireSession("This session expired because you’ve logged out from another tab.");
        }
        return;
    }
});

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
