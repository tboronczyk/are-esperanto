import axios from "axios";
import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";
import LoginView from "../views/Login";
import IndexView from "../views/Index";
import PagesView from "../views/Pages";
import TranslationsView from "../views/Translations";
import AccountsView from "../views/Accounts";
import FilesView from "../views/Files";
import ProgramView from "../views/Program";
import SettingsView from "../views/Settings";
import RegistrationsView from "../views/Registrations";
import PasswordView from "../views/Password";
import NotFoundView from "../views/NotFound";

Vue.use(VueRouter);

const isAuthenticated = () => {
    if (!store.getters["auth/isAuthTokenExpired"]) {
        return Promise.resolve(true);
    }

    // attempt to renew the authToken 
    return axios.post("/api/auth/renew")
        .then((response) => {
            return store.dispatch("auth/setAuthToken", response.data.authToken);
        })
        .then(() => {
            return Promise.resolve(true);
        })
        .catch((error) => {
            return Promise.resolve(false);
        });
};

const requireAuth = async (to, from, next) => {
    if (await isAuthenticated()) {
        next();
        return;
    }
    next({name: "login", params: {internal: true}});
};

const prohibitAuth = async (to, from, next) => {
    // never restrict if "internal"
    if ((to.params && to.params.internal) || !(await isAuthenticated())) {
        next();
        return;
    }
    next({name: "index"});
};

export default new VueRouter({
    mode: "history",
    routes: [
        {
            name: "login",
            path: "/admin/login",
            component: LoginView,
            beforeEnter: prohibitAuth
        },
        {
            name: "index",
            path: "/admin",
            component: IndexView,
            beforeEnter: requireAuth
        },
        {
            name: "pages",
            path: "/admin/pages",
            component: PagesView,
            beforeEnter: requireAuth
        },
        {
            name: "translations",
            path: "/admin/translations",
            component: TranslationsView,
            beforeEnter: requireAuth
        },
        {
            name: "accounts",
            path: "/admin/acounts",
            component: AccountsView,
            beforeEnter: requireAuth
        },
        {
            name: "files",
            path: "/admin/files",
            component: FilesView,
        },
        {
            name: "program",
            path: "/admin/program",
            component: ProgramView,
            beforeEnter: requireAuth
        },
        {
            name: "settings",
            path: "/admin/settings",
            component: SettingsView,
            beforeEnter: requireAuth
        },
        {
            name: "registrations",
            path: "/admin/registrations",
            component: RegistrationsView,
            beforeEnter: requireAuth
        },
        {
            name: "password",
            path: "/admin/password",
            component: PasswordView,
            beforeEnter: requireAuth
        },
        {
            name: "not-found",
            path: "*",
            component: NotFoundView
        }
    ]
});
