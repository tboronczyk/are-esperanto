import axios from "axios"
import NotifierService from "../notifier";
import router from "../../router"
import store from "../../store"

export default {
    logout() {
        store.dispatch("app/setIsLoading", true);

        store.dispatch("auth/clearAuthToken")
            .then(() => {
                // delete refreshToken cookie
                return axios.delete("/api/auth");
                })
                .finally(() => {
                    NotifierService.send("logout");
                    router.push({name: "login", params: {internal: true}});
                    store.dispatch("app/setIsLoading", false);
                });
    }
};
